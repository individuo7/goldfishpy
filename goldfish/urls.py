'''
Created on Jan 6, 2015

@author: bryanfarris
'''
from django.conf.urls import url

from goldfish.views import TestView
from goldfish.views import RedisView
from goldfish.views import ConfigurationView
from goldfish.views import PushView

urlpatterns = [
    # UNDOCUMENTED API
    url(r'^test/$', TestView.as_view()),

    # REDIS CONTROL
    url(r'^redis/$', RedisView.as_view()),
    url(r'^redis/(?P<command>[\w:|-]+)/$', RedisView.as_view(),
        name='Redis Command'),
    url(r'^id_fields/$', ConfigurationView.as_view(),
        {'type': 'id_field'}, name="configure_id_fields"),
    url(r'^id_fields/(?P<resource>[\w:|-]+)/$', ConfigurationView.as_view(),
        {'type': 'id_field'}, name="view_id_fields"),  # GET ONLY
    url(r'^push/$', PushView.as_view(), name='Send Push Notif')
]
