=====
Goldfish
=====

Goldfish is a framework for creating a REST API based on Redis

Quick start
-----------

1. Add "goldfish" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'goldfish',
    )

2. Include the goldfish URLconf in your project urls.py like this::

    url(r'^goldfish/', include('goldfish.urls')),
