'''
Created on Jan 6, 2015

@author: bryanfarris
'''


from api.store import redis
from api.store import redis_master

import random
from datetime import datetime
#import api
#from api.views import ResourceView
from goldfish import signals
import time

class APIManager ():

    @classmethod
    def delete_by_search(cls, query):

        keys = redis.keys(query)
        if len(keys) > 0:
            redis.delete(*keys)

    
    @classmethod
    def post_activity (cls, **kwargs):
        
        activity = {}
        story_type = random.randint(0,3)
        summaries = ['verified an action.','unlocked a level.', 'completed a module.',  'followed a channel.']
        
        activity['summary'] = summaries[story_type]
        
        activity['users'] = ['USERS:' + str(random.randint(2,13)) + '|h']
        activity['timestamp'] = datetime.strftime(datetime.utcnow(), "%Y-%m-%dT%H:%M:%S +0000")
        if story_type == 0:
            action_type = random.randint(0,1)
            if action_type == 0:
                activity['image_name'] = 'BooksIcon'
                activity['action_description'] = 'Read for ' + str(random.choice(['10','15','20','30','40','45','60'])) + ' minutes'
            elif action_type == 1:
                activity['image_name'] = random.choice(['Temp-TakePill'])
                activity['action_description'] = 'Took Mastermind'
        elif story_type == 1:
            level = random.choice(['L1-Trainer','L2-Coach','L3-Mentor','L4-Master'])
            activity['image_name'] = 'Circle'+level[2:]
            activity['action_title'] = level
            activity['action_description'] = 'Modules 1-'+str(random.randint(5,18))
        elif story_type == 2:
            activity['image_name'] = random.choice(['Temp-Day1',
                                                    'Temp-Day2',
                                                    'Temp-Day3',
                                                    'Temp-Day4',
                                                    'Temp-Apolo',
                                                    'Temp-RodProfile',
                                                    'Temp-DeanGrey',
                                                    'Temp-Upcoming',
                                                    'Temp-Bree',
                                                    'Temp-Craig',
                                                    'Temp-DeanMachine',
                                                    'Temp-Delta',
                                                    'Temp-Jeremy',
                                                    'Temp-John',
                                                    'Temp-Molly',
                                                    'Temp-Nadia',
                                                    'Temp-Thor',
                                                    'Temp-Tim'])
            if activity['image_name'][-4:][:-1] == 'Day':
                activity['image_path'] = 'posts/'
            else:
                activity['image_path'] = 'channels/'
            activity['action_title'] = random.choice(['Module ', 'Day '])
            if activity['action_title'] == 'Day ':
                activity['action_title'] += str(random.randint(1,28))
            else:
                activity['action_title'] += str(random.randint(1,12))
            
            activity['action_description'] = random.choice(['Peak State and High Performance\nIntroduction & Overview',
                                                            'Model of the World',
                                                            'Modeling of Excellence',
                                                            'Creating and Collapsing Anchors'])
        elif story_type == 3:
            channel_index = random.randint(0,12)
            activity['image_name'] = ['Apolo.png',
                                      'Rod.png',
                                      'Dean.png',
                                      'Audience.png',
                                      'Plane.png',
                                      'Jump.png',
                                      'Single.png',
                                      'Surfer.png',
                                      'Group.png',
                                      'Yoga.png',
                                      'Emily2.png',
                                      'Swimmer.png',
                                      'Temp-Bree'][channel_index]
            activity['image_path'] = 'channels/'
            activity['action_title'] = ['Peak Performance',
                                        'Founder Updates', 
                                        'Network Science', 
                                        'Event News',
                                        'Epic Lifestyle', 
                                        'Overcoming Objections',
                                        'Successful Singles Tips',
                                        'Team Building',
                                        'Successful Couples Tips',
                                        'Allysian Fitness',
                                        'Meditation',
                                        'Leadership',
                                        'Nutrition'][channel_index]
            activity['action_description'] = random.choice(['Building Your Peak Performance',
                                              'Leading Tips & Tricks',
                                              'Uncovering Hidden Talents',
                                              'Achieving Emotional Balance',
                                              'Tackling Tough Times',
                                              'Discover Your Inner Drive'])
        
        cls.post('activities',[activity], **kwargs)
#                             {'summary':'read a book',
#                              'users':['USERS:2|h'],
#                              'image_name':'Temp-Apolo',
#                              'image_path':'channels/',
#                              'timestamp': "3/10/15, 5:31 AM",
#                              'action_title': 'Mindhack - T1',
#                              'action_description': 'Day 1'}], **kwargs)
    
    @classmethod
    def get (cls, resource, resource_id, *args, **kwargs):
        params = cls.params(resource, resource_id, *args, **kwargs)
        return cls.get_resource(**params)
    
    
    @classmethod
    def post (cls, resource, objects, *args, **kwargs):
        
        params = cls.params(resource, None, *args, **kwargs)
        
        return cls.update_resource({}, objects, True, *args, **params)
    
    
    @classmethod
    def put (cls, resource, resource_id, obj, *args, **kwargs):
        
        params = cls.params(resource, resource_id, *args, **kwargs)
        
        return cls.update_resource({}, obj, True, *args, **params)
    
    
    
    @classmethod
    def get_resource(cls, *args, **kwargs):
        """
        Validate GET request and return resource
        
        Required Kwargs:
        -- resource
        -- id_field
        
        Optional Kwargs:
        -- resource_id (passed as value of id_field)
        
        """
        #Validate required args
        resource = kwargs.pop('resource')
        id_field = kwargs.pop('id_field')
        resource_id = kwargs.get(id_field)
        prefix = cls.prefix(**kwargs)
        
        if resource is None:
            return None
        elif resource_id is None:
            return ResourceManager.list_resources(prefix, resource, **kwargs)

        obj = ResourceManager.get_resource(prefix, resource, resource_id, **kwargs)
        if obj == {}:
            return None #SHOWS 204 NO CONTENT for urls accessing non-existent attributes.
        
        return obj
    

    @classmethod
    def delete_resource(cls, resource, resource_id, *args, **kwargs):
        ResourceManager.delete_resource("", resource, resource_id, **kwargs)
    
    @classmethod
    def update_resource(cls, query_params, data, overwrite, *args, **kwargs):
        """
        Validate POST/PUT request and update resource
        
        Required Kwargs:
        -- resource
        -- id_field
        
        Optional Kwargs:
        -- resource_id (passed as value of id_field)
        """
        #Validate required args
        resource = kwargs.get('resource')
        id_field = kwargs.get('id_field')
        resource_id = kwargs.get(id_field)
        prefix = cls.prefix(**kwargs) 
        style = kwargs.get('style')
        include = kwargs.get('include')
        exclude = kwargs.get('exclude')
        mapping = kwargs.get('mapping')
        delete = kwargs.get('delete')
        return_object = kwargs.get('return_object')
        datatypes = kwargs.get('datatypes')
        print_logs = kwargs.get('print_logs')
        
        #update query params
        if query_params is None:
            query_params = {}
        query_params['style'] = style
        query_params['include'] = include
        query_params['exclude'] = exclude
        query_params['envelope'] = resource
        query_params['mapping'] = mapping
        query_params['delete'] = delete
        query_params['datatypes'] = datatypes
        query_params['return_object'] = return_object
        query_params['print_logs'] = print_logs
        
        if resource is None:
            return None
        
        items = []
        if isinstance(data, list): #HANDLE ARRAY POST
            items = data
        elif isinstance(data, dict): #HANDLE DICT POST/PUT
            items = [data]
        elif isinstance(data, str):
            items = [data]
            
        resources = []
        for item in items:
            #UPDATE RESOURCE
            obj = ResourceManager.update_resource(prefix, 
                                                  resource, 
                                                  id_field, 
                                                  resource_id, 
                                                  overwrite,
                                                  query_params,
#                                                  style=style,
#                                                  include=include,
#                                                  exclude=exclude,
                                                  **item)
            if obj is not None:
                resources.append(obj)
        
        if resources == []:
            return None
        
        elif isinstance(data, dict): #HANDLE DICT POST/PUT
            return resources[0]
        
        return resources
        
    
    
    @classmethod
    def prefix (cls, **kwargs):
        """
        Return a resource prefix.
        
        Required Args/Kwargs:
        -- resource_tree
        """
        
        resource_tree = kwargs.get('resource_tree')
        prefix = ''
        
        if resource_tree is not None:
            for resource_dict in resource_tree:
                if isinstance(resource_dict, dict):
                    for resource, id_field in resource_dict.items():
                        resource_id = kwargs.get(id_field)
                        prefix = ResourceManager.hashkey(prefix, resource, resource_id) + ':'
                elif isinstance(resource_dict, str):
                    prefix = prefix + resource_dict + ':'
               
        return prefix
    
    @classmethod
    def params (cls, resource, resource_id, *args, **kwargs):
        
        #GENERATE PARAMS:
        params = kwargs
        params['resource'] = resource
        
        id_field = ResourceManager.id_field(resource, None)
        params['id_field'] = id_field
        if resource_id:
            params[id_field] = resource_id
            
        
        #GENERATE RESOURCE_TREE:
        tree = []
        for arg in args:
            r = list(arg.keys())[0]
            v = arg[r]
            
            #Add Id Field To Resource Tree
            field = ResourceManager.id_field(r, None)
            tree.append({r:field})
        
            #Add Id data to Params
            params[field] = v
        
        if tree == []:
            tree = [{}]
            
        params['resource_tree'] = tree
        
        return params


#        #GENERATE PARAMS:
#        params = kwargs
#        params['resource'] = resource
#        
#        id_field = ResourceManager.id_field(resource, None)
#        params['id_field'] = id_field
#        if resource_id:
#            params[id_field] = resource_id
#            
#        
#        #GENERATE RESOURCE_TREE:
#        tree = []
#        for arg in args:
#            r = list(arg.keys())[0]
#            v = arg[r]
#            
#            #Add Id Field To Resource Tree
#            field = ResourceManager.id_field(r, None)
#            tree.append({r:field})
#        
#            #Add Id data to Params
#            params[field] = v
#        
#        if tree == []:
#            tree = [{}]
#            
#        params['resource_tree'] = tree
#        
#        #APPEND DEFAULT STYLE
##        params = cls.append_styles(resource, resource_id, params)
#        
#        #OVER-RIDE ANY PARAMS PASSED BY KWARGS (ALLOWS FOR CUSTOM STYLES AND FILTERING)
#        params.update(kwargs)
#        
#        return cls.get_resource(**params)


class ResourceManager ():
    #UPDATES REQUIRED:
    # Auxilary sets (e.g. hardcoded)
    # Handle envelopes etc. properly
    # Add sub resources (allow add by set, list, zset, etc.)
    # Support parent resources
    # Delete sub resources
    @classmethod
    def update_resource(cls, 
                        prefix, 
                        resource, 
                        id_field, 
                        resource_id, 
                        overwrite, 
                        query_params, 
#                        style='verbose', 
#                        include=None, 
#                        exclude=None, 
                        **kwargs):
        """
        Updates resource with given information, overwriting existing information if passed.
        
        If needed, creates a new resource.
        
        Required Args/Kwargs:
        --  resource
        --  resource_id
        --  id_field
        """
        #Validate input:
        if resource is None:
            return None
        
        #DETERMINE RESOURCE ID:
        #Defaults to ID in url if included
        if resource_id is None:  
            if id_field is not None and kwargs.get(id_field) is not None:  #If no id in URL, check if it exists in resource body
                resource_id = kwargs.get(id_field)
            else:
                #resource_id = ResourceManager.next_id(prefix, resource) #Otherwise generate resource id with redis incr
                model_obj = signals.pre_create.send(sender=cls.__class__, instance=cls, hashkey=prefix, data=kwargs, resource=resource, resource_id=resource_id)

                if len(model_obj) > 0 and len(model_obj[0]) > 1 \
                        and resource.upper() != 'ACTIVITIES' and model_obj[0][1] is not None:  # TEMP remove Activities after added to model
                    resource_id = model_obj[0][1].id
                else:
                    resource_id = ResourceManager.next_id(prefix, resource)

        
        if id_field is not None: 
            kwargs[id_field] = resource_id  #Assign resource_id to obj
            cls.id_field(resource, id_field)  #Keep record of id field used for each resource
        
        if overwrite:
            #DELETE EXISTING RESOURCE TO ENSURE OVERWRITE
            #TODO: ENSURE THIS WORKS WITH NEW DELETE METHODS & CLEARS FROM PARENT OBJECTS LISTS
            if kwargs.get('prefix') is not None: #ERROR PROTECTION FOR KWARGS:
                kwargs.pop('prefix')
            if kwargs.get('resource') is not None:
                kwargs.pop('resource')
            if kwargs.get('resource_id') is not None:
                kwargs.pop('resource_id')
                
            ResourceManager.delete_resource(prefix, resource, resource_id, **kwargs)
        
        
        hashkey = cls.hashkey(prefix, resource, resource_id)
#        setkey = cls.setkey(prefix, resource)
        zsetkey = cls.zsetkey(prefix, resource)
        
        #Create resource hash in redis
        RedisHelper.store_object(hashkey, obj=kwargs, **query_params)

        signals.post_update.send(sender=cls.__class__, instance=cls, hashkey=hashkey, data=kwargs, resource=resource, resource_id=resource_id)
        
        #Create resource sets
#        redis.sadd(setkey,hashkey)

        #Create resource zsets
        # zscore = resource_id
        # if not Common.is_number(resource_id): #ERROR PROTECTION
        #     zscore = redis.zcard(zsetkey)
        zscore = int(time.time())
        
        redis.zadd(zsetkey,hashkey,zscore)
        
        #Add to overall resource set:
        resource_zsetkey = cls.zsetkey('', resource)
        redis.zadd(resource_zsetkey,hashkey, zscore)
#        resource_setkey = cls.setkey('', resource)
#        redis.sadd(resource_setkey,hashkey)
        
        #Notify Parent Resource
        parenthash = prefix[:-1] #remove trailing :
        #print('SETTING PARENT HASH',parenthash)
        redis.hset(parenthash,resource,zsetkey)

        #Set parent's inner object
        words = parenthash.split(':')
        while parenthash.strip() != '' and len(words) > 0 and '|' not in words[-1]:
            field = words[-1]
            childhash = parenthash
            parenthash = ':'.join(words[:-1])

            redis.hset(parenthash, field, childhash)
            words = parenthash.split(':')

        
        if kwargs.get('style') == 'hashkey':
            return hashkey #return hashkey (used for storing nested objects)

        if query_params.get('return_object') is not None and not query_params.get('return_object'):
            print ('BLOCKING RETURN OF OBJECT')
            return

        print ('RETURNING OBJECT AFTER UPDATE RESOURCE CALL')
        return cls.get_resource(prefix,
                                resource, 
                                resource_id, 
#                                style=kwargs.get('style','verbose'),
#                                include=kwargs.get('include'),
#                                exclude=kwargs.get('exclude'),
                                **query_params)
            
#        #Validate input:
#        if resource is None or resource_id is None:
#            return None
#        
#        hashkey = cls.hashkey(prefix, resource, resource_id)
#        setkey = cls.setkey(prefix, resource)
#        #Generate resource specific keys:
##        id_field = kwargs.get('id_field')#resource + '_id'
#        
#        #Create resource hash in redis
#        if id_field is not None:
#            kwargs[id_field] = resource_id
#        #sub resources:
##        kwargs['profiles'] = USER_ACCOUNT_HASH_KEY + str(user_id) + PROFILE_SET_KEY #Include key to profiles set so that profiles will be recursively deleted when a user is deleted
#        RedisHelper.store_object(hashkey, obj=kwargs)
#        
#        
#        #Create resource sets
#        redis.sadd(setkey,hashkey)
#        
#        #Add to overall resource set:
#        resource_setkey = cls.setkey('', resource)
#        redis.sadd(resource_setkey,hashkey)
#        
#        #And to overall resource id set:
##        resource_id_setkey = cls.setkey('', resource+'_id')
##        redis.sadd(resource_id_setkey,resource_id)
#        
#        #Notify Parent Resource
##        if prefix != '': #TEMP
#        parenthash = prefix[:-1] #remove trailing :
#        print('SETTING PARENT HASH',parenthash)
#        redis.hset(parenthash,resource,setkey)
#        
#        #Auxilary sets:
##        #Build test users set:
##        if kwargs.get('hardcode') is not None:
##            redis.sadd(HARDCODED_NEARBY_USERS,hashkey)
#        
#        
#        return cls.get_resource(prefix, resource, resource_id)
    

    
    @classmethod
    def get_resource(cls, prefix, resource, resource_id, **kwargs):
        """
        Retrieves resource for given resource_id
        
        Required Args/Kwargs:
        -- resource
        -- resource_id
        
        Optional Args/Kwargs:
        -- style= default|verbose|hashkey|id  #TODO respect style param
        """
        #Validate input:
        if resource is None or resource_id is None:
            return None

        # if 'return_object' in kwargs and kwargs.get('return_object') == False:
        #     print ('BLOCKING RETURN OF OBJECT')
        #     return
        
        hashkey = cls.hashkey(prefix, resource, resource_id)
        kwargs['parents'] = [resource]
        kwargs['resource'] = resource
        return RedisHelper.restore_object(hashkey, **kwargs)
#                                          max_depth=kwargs.get('max_depth'), 
#                                          exclude=kwargs.get('exclude'))
    
    

    @classmethod
    def list_resources(cls, prefix, resource, **kwargs):
        """
        Lists resources with optional filters
        
        Required Args/Kwargs:
        -- resource
       
        Optional Args/Kwargs:
        -- style= default|verbose|hashkey|id|url  #TODO respect style param
        """
        #Validate input:
        if resource is None:
            return None
        
        kwargs['parents'] = [resource]
        kwargs['resource'] = resource
        resources = RedisHelper.restore_object(cls.zsetkey(prefix,resource), **kwargs)
        envelope = resource.upper()
        return {envelope: resources}
#        return RedisHelper.list_objects(#setkey=cls.setkey(prefix,resource),
#                                        zsetkey=cls.zsetkey(prefix,resource), 
#                                        envelope=resource.upper(), **kwargs)
        

    
    @classmethod
    def delete_resource(cls, prefix, resource, resource_id, **kwargs):
        """
        Deletes resource for given resource_id
        
        Required Args/Kwargs:
        -- resource
        -- resource_id
        
        """
        
        #Validate input:
        if resource is None or resource_id is None:
            return None
        
        hashkey = cls.hashkey(prefix, resource, resource_id)
        
        #TODO: BUILD DELETE SUB-RESOURCES FUNCTIONS
        #DELETE ALL SUB-RESOURCES (ADHERE TO DELETE RULES)
        #Allow delete by cascade, nullify, etc.
        #use list_resources with style=id to retrieve ids for a sub-resource & 
        # subsequently pass them to delete_resource. 
        
        #REMOVE FROM ZSETS:
        # prefix_zsetkey = cls.zsetkey(prefix, resource)
        # redis.zrem(prefix_zsetkey,hashkey)
        
        # resource_zsetkey = cls.zsetkey('', resource)
        # redis.zrem(resource_zsetkey,hashkey)
        
        #REMOVE FROM SETS:
#        prefix_setkey = cls.setkey(prefix, resource)
#        redis.srem(prefix_setkey,hashkey)
#        
#        resource_setkey = cls.setkey('', resource)
#        redis.srem(resource_setkey,hashkey)
        #response = cls.get_resource(prefix, resource, resource_id, **kwargs)

        RedisHelper.delete_object(key=hashkey, parents=[resource], **kwargs)
        signals.post_delete.send(sender=cls.__class__, instance=cls, hashkey=hashkey, resource=resource, resource_id=resource_id)
        
        return {}
    
#    def item_id (self, item, **kwargs):
#        """
#        Returns item id for a given resource item dictionary
#        """
#        key = resource + ':ids'
#        return str(redis.incr(key))


    @classmethod
    def id_field (cls, resource, id_field, **kwargs):
        """
        Returns id_field for given resource if stored.
        
        Optionally pass id_field arg to overwrite existing value.
        
        If None is passed for both parameters, return all ids in a dict.
        """
        
        if id_field is not None:
            redis.hset('ID_FIELDS',resource,id_field)
            return id_field
        elif resource is not None:
            return redis.hget('ID_FIELDS',resource)
        else:
            return redis.hgetall('ID_FIELDS')
        
        
    @classmethod
    def next_id (cls, prefix, resource, **kwargs):
        """
        Returns next id for a specific resource given redis incr.
        
        All ids are guaranteed to be new: method verifies that 
        resource doesn't already exist before returning.
        """
        
        
        key = prefix + resource.upper() + ':ids' #TODO: DELETE THIS INCR KEY WHEN DELETING A RESOURCE SET
        
        resource_id = str(redis.incr(key))
        
        #Verify that implied hashkey is not already in overall resource id set:
        hashkey = cls.hashkey(prefix, resource, resource_id)
        resource_zsetkey = cls.zsetkey('', resource)
        if redis.zscore(resource_zsetkey, hashkey) is not None:
#        resource_setkey = cls.setkey('', resource)
#        if redis.sismember(resource_setkey, hashkey):
            return cls.next_id(prefix, resource) #SKIP & Incr again
        
        return resource_id

    @classmethod
    def hashkey (cls, prefix, resource, resource_id, **kwargs):
        """
        Returns hashkey for resource
        """
        
        prefix = prefix+resource.upper() + ':'
        return prefix + str(resource_id)+'|h'
    
    
    @classmethod
    def setkey (cls, prefix, resource, **kwargs):
        """
        Returns setkey for resource
        """
        
        return prefix + resource.upper() + '_SET:' +'|s'
    
    @classmethod
    def zsetkey (cls, prefix, resource, **kwargs):
        """
        Returns Zsetkey for resource
        """
        
        return prefix + resource.upper() + '_ZSET:'+'|z'
    
    @classmethod
    def listkey (cls, prefix, resource, **kwargs):
        """
        Returns listkey for resource
        """
        
        return prefix + resource.upper() + '_LIST:'+'|l'
    
    
    
    
    
    
    
    
    
    
class RedisHelper ():
    
    @classmethod
    def lookup (cls, key=None, **kwargs):  #TO DEPRECATE -- mostly same as restore_object
        
        if key is None: 
            return
        
        recursive = kwargs.get('recursive',None)#'no')
        kwargs['embedded'] = recursive
        
        datatype = redis.type(key)  
        if datatype == 'set':  #Redis set detected (list)
            obj = cls.restore_set(key)
        elif datatype == 'zset':
            obj = cls.restore_zset(key) 
        elif datatype == 'list':
            obj = cls.restore_list(key)
        elif datatype == 'hash':
                obj = cls.restore_hash(key)
        else:
            if key == '_True_':
                obj = True
            elif key == '_False_':
                obj = False
            elif kwargs.get('embedded'):
                # key does not represent an embedded object...it is the object!
                obj = key #String, int, long, float, complex
            else:
                print('redis.get: ' + key)
                obj = redis.get(key)
                
        if kwargs.get('url'):
            return [obj,'https://bridgex.herokuapp.com/redis/?a=lookup&url=y&key='+key]
            
        return obj
        
        #OLD (DEPRECATED):
        data = None
        objects = []
        #HANDLE VARIOUS TYPES OF DATA STORAGE:
        datatype = redis.type(key)  
        if datatype == 'set':  #Redis set detected
            data = redis.smembers(key)
            
            
        elif datatype == 'zset':
            data = redis.zrange(key, 0, -1)
        elif datatype == 'list':
            data = redis.lrange(key, 0,  -1)
        elif datatype == 'hash':
            data = cls.restore_hash(key) #Recursively assemble objects within dict
        else: # entry appears to be a string/int
            print('redis.get: ' + key)
            data = redis.get(key)
            
        if datatype == 'set' or datatype == 'zset' or datatype == 'list':
            if recursive == 'yes':
                for k in data:
                    obj = cls.lookup(key=k, **kwargs)
                    if obj is not None:
                        objects.append(obj)
                    else:
                        objects.append(data)
            else:
                objects = data
        else:
            if recursive == 'yes':
                objects.append(data)
            else:
                objects = data
                
        return objects
    
    @classmethod
    def keys (cls, **kwargs):
        
        pattern = kwargs.get('pattern','*')
        
        if kwargs.get('url'):
            keylinks = []
            for key in redis.keys(pattern):
                keylink = key+ '  https://bridgex.herokuapp.com/redis/?a=lookup&url=y&key='+key
                keylinks.append(keylink)
            return keylinks
        
        return redis.keys(pattern)
    
    @classmethod
    def clear_keys_with_pattern (cls, **kwargs):
        
        keys = cls.keys(**kwargs)
        
        for k in keys:
            redis.delete(k)
            
        return cls.keys(**kwargs)
    

    @classmethod
    def store_object (cls, key, obj, **kwargs):
        embedded = kwargs.get('_embedded') #Internally used arg that should not be passed by callers.  
        #This is used so that if an object is embedded, you can store it and return it's hashkey for storage in it's parent object.
        
        #Build List Of Parent Resources:
        parents = kwargs.get('parents',[])[:]
        if parents is None and kwargs.get('envelope') is not None:
            parents = [kwargs.get('envelope').lower()] #First Parent is Envelope
        elif isinstance(parents,list):
            resource_tree = cls.resource_tree(key)
            if len(resource_tree) >= 1:
                new_parent = resource_tree[-1] #last index is resource for key
                parents.append(new_parent)
        kwargs['parents'] = parents
        
        
        
        if isinstance(obj, (str, int, float, complex)):
            if embedded:
                return obj
            else:
                key = key +'|v'
                redis.set(key, obj)
                return obj
                
        elif isinstance(obj,bool):
            if obj:
                bool_val = '_True_'
            else:
                bool_val = '_False_'
            if embedded:
                return bool_val
            else:
                key = key +'|v'
                redis.set(key, bool_val)
                return bool_val
            
        elif isinstance(obj, (list,tuple)):
            
            if embedded:  #This is a list of nested objects, therefore they should be posted to update resource as if they came from a POST/PUT
                prefix = kwargs.get('_prefix')
                resource = kwargs.get('_resource')
                id_field = ResourceManager.id_field(resource, None)
#                setkey = ResourceManager.setkey(prefix,resource)
                zsetkey = ResourceManager.zsetkey(prefix, resource)
#                i = 0
                for item in obj:
                    if isinstance(item, dict):
                        parents = kwargs.get('parents',[])
                        if resource in parents: #If new resource already exists, it is recursive return key
                            continue
                        
                        ResourceManager.update_resource(prefix, 
                                                        resource, 
                                                        id_field, 
                                                        None, 
                                                        False, 
                                                        {'style':'hashkey',
                                                         'parents':parents}, 
                                                        **item)
                    else:
                        mapping_resource = cls.get_mapping_resource(key, **kwargs)
                        if mapping_resource is not None:
                            #LIST OF REFERENCE IDS, SAVE AS SET
                            params = {}
                            params['prefix'] = ''
                            params['resource'] = mapping_resource
                            key = key + '|s'
                            cls.store_set(key, obj, **params)
                            return key
                        else:
                            #NOT A LIST OF OBJECT DICTS
                            key = key +'|l'
                            cls.store_list(key, obj, **kwargs) #TODO: Add Kwargs?
                            return key
                return zsetkey#setkey        
            else:
                mapping_resource = cls.get_mapping_resource(key, **kwargs)
                if mapping_resource is not None:
                    #LIST OF REFERENCE IDS, SAVE AS SET
                    params = {}
                    params['prefix'] = ''
                    params['resource'] = mapping_resource
                    key = key + '|s'
                    cls.store_set(key, obj, **params)
                    return key
                else:
                    key = key +'|l'
                    cls.store_list(key, obj, **kwargs)
                    return key  #ADDED THIS SO THAT NESTED PARENT RESOURCES CAN HAVE THEIR HASHKEYS STORED IN A LIST WITHOUT STORING MORE OBJS

#                    elif isinstance(item, (list,tuple)):  
#                        listkey = resource +':'+str(i) 
#                        cls.store_list(listkey, item)
                        
#                        embedded_key = prefix+
#                        redis.sadd(setkey, embedded_key)
#                        cls.store_object(embedded_key, obj, _embedded=True, _prefix=prefix)
#                    i += 1
                
            
            
#            cls.store_list(key, obj)
#            if embedded:
#                return key
                
        elif isinstance(obj, dict):
#            key = key +'|h'  #ALREADY APPENDED BY RESOURCE MANAGER
            cls.store_hash(key, obj, **kwargs)
            if embedded:
                return key
            
    @classmethod
    def store_hash (cls, hashkey, hash_obj, **kwargs):

        #STORE VALUES IN REDIS HASH
        for key in hash_obj.keys():
            
            obj = hash_obj.get(key)
            
            if obj is None: #ERROR PROTECTION
                continue
            
            #CREATE REDIS ENTRY
            embedded_key = hashkey + ':' + key #+ '|h'
            prefix = hashkey + ':'
            
#            _embedded = True #Pass embedded to look for nested objects
#            #Do not pass embedded for recursive relationships 
#            parent_resources = cls.resource_tree(hashkey)
#            if key in parent_resources:
#                _embedded = False

            kwargs['_embedded'] = True
            kwargs['_prefix'] = prefix
            kwargs['_resource'] = key
            
            #TODO: Possible DB memory leak: what happens if an object
            # is already stored at the given key. Are were sure it will be
            # overwritten? Is it possible that nested lists and objects
            # are not properly deleted during overwrite?
            redis.hset(hashkey,key,cls.store_object(embedded_key, 
                                                    obj, **kwargs))
#                                                    _embedded=_embedded, 
#                                                    _prefix=prefix, 
#                                                    _resource=key))
            
            
            
        
#            RedisHelper.delete_object(key=embedded_key) #clear dict if it existed before #CHANGED  #TODO: DO WE WANT THIS GIVEN PUT/POST DIF?
            
#            if isinstance(obj, (list,tuple)):
                
                
                #TODO: VERY SIMILAR TO CODE IN UPDATE RESOURCE...FIND WAY TO ABSTRACT/MERGE
                #TREAT AS A SUB-RESOURCE (use upper case resource name in hashkey, include 
#                prefix = hashkey + ':'
#                resource = key
##                resource_id = ResourceManager.next_id(resource)
#                embedded_key = prefix + resource.upper() #ResourceManager.hashkey(prefix, resource, '')
#                
#                #Create resource sets
#                setkey = ResourceManager.setkey(prefix, resource)
#                redis.sadd(setkey,embedded_key)
#        
#                #Add to overall resource set:
#                resource_setkey = ResourceManager.setkey('', resource)
#                redis.sadd(resource_setkey,embedded_key)
                
                
#                ResourceManager.update_resource(prefix, resource, id_field, item_id, **item)
#                cls.store_list(embedded_key, obj)
#                redis.hset(hashkey,key,embedded_key)#, embedded=True))
#                return
#            redis.hset(hashkey,key,cls.store_object(embedded_key, obj, _embedded=True))
    
    @classmethod
    def store_list (cls, listkey, simple_list, **kwargs):

        #STORE VALUES IN REDIS LIST
        i = 0
        for item in simple_list:
            embedded_key = listkey+':'+str(i) #+ '|l'
            kwargs['_embedded'] = True
            
            #Check Against List of Parent Resources: #TODO: DEPRECATE, this is unnecessary
#            resource_tree = cls.resource_tree(item)
#            if len(resource_tree) >= 1:
#                new_parent = resource_tree[-1] #last index is resource for key
#                parents = kwargs.get('parents',[])
#                if new_parent in parents: #If new resource already exists, it is recursive return key
#                    redis.rpush(listkey, item)
#                    i+=1
#                    continue
            
            redis.rpush(listkey, cls.store_object(embedded_key, 
                                                  item, 
                                                  **kwargs))
#                                                  _embedded=True 
#                                                  _resource='list' #Used only in case of a list embedded inside a list
#                                                  )) #CREATE REDIS ENTRY
            i+=1
            
            
            #By passing embedded=True, we instruct store object to only store comlex datatypes,
            #Simple data types that can be stored in the list directly will be returned without being stored elsewhere
            #Complex objects will be stored separately and the key to access them will be returned & stored in this list
            #We pass embedded listkey to all items because only the embedded objects will become stored

    @classmethod
    def store_set (cls, setkey, simple_list, **kwargs):
        prefix = kwargs.get('prefix', '')
        resource = kwargs.get('resource', '')
        params = {}
        #STORE VALUES IN REDIS SET
        for item in simple_list:
            key = str(item)
            if key[0] == '-':
                hashkey = ResourceManager.hashkey(prefix, resource, key[1:], **params)
                redis.srem(setkey, hashkey)
            else:
                hashkey = ResourceManager.hashkey(prefix, resource, item, **params)
                redis.sadd(setkey, hashkey)

    
    @classmethod
    def restore_object_old (cls, key, **kwargs):
        
        depth = kwargs.get('depth',0) #Current depth, initiates at zero (but immediately increments)
        if str(depth) == kwargs.get('max_depth'):
            return key #if you have reached max depth, return the key of the next layer
        kwargs['depth'] = depth + 1 #otherwise, go one level deeper
        
        datatype = kwargs.pop('datatype',cls.datatype(key))
        
        
        
        
        
        
        
        
        
        #Build List Of Parent Resources:
        parents = kwargs.get('parents',[])[:]
        if len(parents) == 0 and kwargs.get('envelope') is not None:
            parents = [kwargs.get('envelope').lower()] #First Parent is Envelope
        elif isinstance(parents,list):
            resource_tree = cls.resource_tree(key)
            if len(resource_tree) >= 1:
                new_parent = resource_tree[-1] #last index is resource for key
#                if new_parent in parents: #If new resource already exists, it is recursive return key
#                    return key
                if new_parent not in parents:
                    parents.append(new_parent)
        kwargs['parents'] = parents
        
        
        
        
#        datatype = redis.type(key)  
#        datatype = cls.datatype(kw)
        
        if datatype == 'set':  #Redis set detected (list)
            obj = cls.restore_set(key, **kwargs)
        elif datatype == 'zset':
            obj = cls.restore_zset(key, **kwargs) 
        elif datatype == 'list':
#            if str(kwargs['depth']) == kwargs.get('max_depth') and kwargs.get('style') != 'verbose': #If this next level is going to be max depth, just return list key unless verbose
#                return key -->TEST MORE THEN IMPLEMENT
            obj = cls.restore_list(key, **kwargs)
        elif datatype == 'hash':
#            if str(depth+1) == kwargs.get('max_depth'):
#                return key
            kwargs['depth'] = depth 
            obj = cls.restore_hash(key, **kwargs)
        else:
            if key == '_True_':
                obj = True
            elif key == '_False_':
                obj = False
            elif kwargs.get('embedded'):
                # key does not represent an embedded object...it is the object!
                obj = key #String, int, long, float, complex
            else:
                print('redis.get: ' + key)
                obj = redis.get(key)
        
        return obj    
#        return {'obj':obj, 'depth':kwargs.get('depth'), 'max_depth':kwargs.get('max_depth')}    
        
    @classmethod
    def restore_object (cls, key, **kwargs):
        resource = kwargs.get('resource', '')
        if kwargs.get('print_logs') is not None:
            print("restore_object: " + key + " for " + resource)
        key = str(key)
        depth = kwargs.get('depth',0) #Current depth, initiates at zero (but immediately increments)
        if str(depth) == kwargs.get('max_depth'):
            return key #if you have reached max depth, return the key of the next layer
        kwargs['depth'] = depth + 1 #otherwise, go one level deeper
        
        datatype = kwargs.pop('datatype', cls.datatype(key))


        

        
        #Build List Of Parent Resources:
        parents = kwargs.get('parents',[])[:]

        if isinstance(parents,list) and cls.datatype(key) == 'hash': #ERROR PROTECTION
#            if len(resource) >= 5 and (resource[-5:] == '_zset' or resource[-5:] == '_list'):
#                resource = resource[:-5]
#                pass
#            el
            if resource not in parents:
                parents.append(resource)
                
            elif len(parents) > 1 and kwargs.get('style') not in ['id','url']: #and cls.datatype(key) == 'hash'
                return key#{'parents': parents, 'resource':resource, 'objects': key} #DO NOT RETURN RECURSIVE RELATIONS 
                #
        kwargs['parents'] = parents
        
        
#        resource = ''
#        
#        
#        #Build List Of Parent Resources:
#        parents = kwargs.get('parents',[])[:]
#        if len(parents) == 0 and kwargs.get('envelope') is not None:
#            parents = [kwargs.get('envelope').lower()] #First Parent is Envelope
#        elif isinstance(parents,list):
#            resource_tree = cls.resource_tree(key)
#            if len(resource_tree) >= 1:
#                resource = resource_tree[-1] #last index is resource for key
##                if new_parent in parents: #If new resource already exists, it is recursive return key
##                    return key
#                if len(resource) >= 5 and (resource[-5:] == '_zset' or resource[-5:] == '_list'):
#                    resource = resource[:-5]
#                    pass
#                elif resource not in parents:
#                    parents.append(resource)
#                elif len(parents) > 1 and cls.datatype(key) == 'hash' and kwargs.get('style') not in ['id','url']:
#                    return {'parents': parents, 'resource':resource, 'objects': key} #DO NOT RETURN RECURSIVE RELATIONS 
#                
#        kwargs['parents'] = parents
        
        
        
        
        
        
        #CHECK FOR STYLE PARAM BEFORE RESTORING FULL OBJECT:
        style = kwargs.get('style','verbose')
        
        if style not in ['hashkey','setkey','count','id','verbose','url','link','seturl']:
            
            #ASSEMBLE NESTED STYLES (FIRST TIME ONLY)
            if kwargs.get('nested_styles') is None:
                try:
                    
                    nested_styles = {}
                    
                    #First check for style definition in styles dict:
                    styles = kwargs.get('styles')
                    if styles is not None and style in styles:
                        nested_styles = styles[style]
                        
                    #If Not found, assemble nested styles definition:
                    if nested_styles == {}:
                        nested_styles = dict(e.split(':') for e in style.split('|'))
                    
                    kwargs['nested_styles'] = nested_styles
                except:
                    pass
#                    return 'ERROR: Invalid style parameter'
            #style = 'verbose' #DEFAULT TO VERBOSE STYLE IF MATCHING NESTED STYLE DOES NOT EXIST
            
            #LOOK FOR NESTED STYLE:
            nested_styles = kwargs.get('nested_styles')
            
            #Create nested key for nested styles
            nested_key = ''
            i = 1
            for r in parents:
                nested_key += r
                if i < len(parents):
                    nested_key += '.'
                i += 1
            if resource not in parents:
                nested_key += '.' + resource
            
            #Create parent key for nested styles (only one level up)
            parent_key = resource
            if len(parents) >= 2:
                parent_key = parents[-2] + '.' + resource#parents[-1]
        
            if nested_styles is not None:# and (key in nested_styles or parent_key in nested_styles or nested_key in nested_styles):
                #Determine style key
                style_key = None
                if resource in nested_styles:
                    style_key = resource
                elif parent_key in nested_styles:
                    style_key = parent_key
                elif nested_key in nested_styles:
                    style_key = nested_key
                
                if style_key is not None:
                    style = nested_styles[style_key]
                    
#                    #CHECK IF NESTED STYLE IS A COMPONENTS STYLE:
#                    components = style.split('_')
#                    if len(components) > 1:
#                        style = components[0]
#                        index_range = components[1].split('-')
#                        if len(index_range) > 1:
#                            first = int(index_range[0])
#                            last = int(index_range[1])
                            
        
            
        if datatype in ['set','zset','list']:
            #COUNT OF set, zset, list
            count = 0
            if datatype == 'set':
                count = redis.scard(key)
            elif datatype == 'zset':
                count = redis.zcard(key)
            elif datatype == 'list':
                count = redis.llen(key)
            #CHECK FOR COMPONENT STYLE:
            first = 0
            last = -1
            if style is not None and len(style.split(':')) == 1: #STYLE IS NOT A NESTED STYLE
                components = style.split('_')
                if len(components) > 2:
                    style = components[0]
                    first = int(components[1])
                    last = int(components[2])
#                components = style.split('_')
#                if len(components) > 1:
#                    style = components[0]
#                    index_range = components[1].split('-')
#                    if len(index_range) > 1:
#                        first = int(index_range[0])
#                        last = int(index_range[1])
                                
            #RESTORE OBJECT: STYLE SHOULD NOW MATCH AN EXISTING STYLE:
            if first != 0 or last != -1:
                if first < 0:
                    first = count + first

                kwargs['offset'] = first
                kwargs['limit'] = last - first + 1
            
            if style == 'setkey':
                return key
            elif style == 'count':
                # if datatype == 'set':
                #     return redis.scard(key)
                # elif datatype == 'zset':
                #     return redis.zcard(key)
                # elif datatype == 'list':
                #     return redis.llen(key)
                return count
                
            elif style == 'seturl':
                obj = cls.set_url(key, **kwargs)
#            elif style in ['id','hashkey']:
#                return None #NO RESOURCE ID FOR A SET/ZSET/LIST
            else:
#                items = []
                wrap_list = False
                if kwargs.get('offset') is not None or kwargs.get('page') is not None:
                    wrap_list = True
                    
                if datatype == 'set':  #Redis set detected (list)
                    obj = cls.restore_set(key, **kwargs)
                elif datatype == 'zset':
                    obj = cls.restore_zset(key, **kwargs) 
                elif datatype == 'list':
                    obj = cls.restore_list(key, **kwargs)
                
                if wrap_list:
                    count = 0
                    if datatype == 'set':
                        count = redis.scard(key)
                    elif datatype == 'zset':
                        count = redis.zcard(key)
                    elif datatype == 'list':
                        count = redis.llen(key)
                        
                    obj = {'count':count,'objects':obj}
#                obj = {'parents': parents, 'resource':resource, 'offset':kwargs.get('offset'), 'limit':kwargs.get('limit'), 'style':style,'style_key':style_key, 'objects': obj}
#                obj = items
                #RESTORE NESTED OBJECTS:
#                obj = [] 
#                for item in items:
#                    restored_item = cls.restore_object(item, **kwargs)
#                    if restored_item is not None:
#                        obj.append(restored_item)
                
        elif datatype == 'hash':
            if style == 'hashkey':
                return key
            elif style == 'id':
                return key.split(':')[-1].split('|')[0]
            elif style == 'count':
                return redis.hlen(key)
            elif style == 'url':
                return cls.object_url(key, **kwargs)
            else:
                kwargs['depth'] = depth

                if 'hashkey' in kwargs:
                    del kwargs['hashkey']

                obj = cls.restore_hash(hashkey=key, **kwargs)
#                obj = {'parents': parents, 'resource':resource, 'offset':kwargs.get('offset'), 'limit':kwargs.get('limit'), 'style':style,'style_key':style_key, 'objects': obj}
                if style == 'link':
                    if isinstance(obj,dict):
                        obj['url'] = cls.object_url(key, **kwargs)
        else:
            if key == '_True_':
                obj = True
            elif key == '_False_':
                obj = False
            elif kwargs.get('embedded'):
                # key does not represent an embedded object...it is the object!
                obj = key #String, int, long, float, complex
            else:
                print('redis.get: ' + key)
                obj = redis.get(key)
        
        return obj
    
    @classmethod
    def paging_indexes (cls, **kwargs):
        """
        Returns 2 item array with first/last index to search in a 
        list/set given paging params
        """
        #PAGING PARAMS:
        page = kwargs.get('page')
        page_size = kwargs.get('page_size',50)
        if not Common.is_number(page_size):
            page_size = 50 #ERROR PROTECTION
        
        #offset & limit params overridden page/page_size params:
        offset = kwargs.get('offset')
        limit = kwargs.get('limit',page_size)
        
        start_index = 0
        last_index = -1
        
        if page is not None and Common.is_number(page):
            offset = int(page)*page_size
            limit = page_size
        if offset is not None and limit is not None:
            offset = int(offset)
            limit = int(limit)
            start_index = offset
            last_index = offset + limit - 1
        
        return [start_index,last_index]
    
    @classmethod
    def restore_list (cls, listkey, **kwargs):
        
#        obj = []
        
#        if redis.type(listkey) != 'list': return redis.get(listkey) #Not a list, return value at key #TODO: enhance to pick other data type

        
        #RETRIEVE VALUES IN REDIS FOR EACH ITEM IN LIST
        page_range = cls.paging_indexes(**kwargs)
        redis_list = redis.lrange(listkey, page_range[0], page_range[1])#0,  -1)
        
        return cls.restore_redis_list(listkey, redis_list, **kwargs)
#        parent_resources = cls.resource_tree(listkey)[:-1]
#        
#        kwargs['embedded'] = True
#        for item in redis_list:
#            #Do not restore recursive relationships
#            if cls.datatype(item) == 'hash':
#                item_resource = cls.resource_tree(item)[-1] 
#                if item_resource in parent_resources:
#                    obj.append(item)
#                    continue
#            obj.append(cls.restore_object(item,**kwargs))
#            
#        return obj
    
    @classmethod
    def restore_set (cls, setkey, **kwargs):        
#        obj = []
        #NOTE: PAGING DOES NOT AFFECT SETS

        #RETRIEVE VALUES IN REDIS FOR EACH ITEM IN LIST
#        range = cls.paging_indexes(**kwargs) #range[0], range[1])#
        resource = cls.get_mapping_resource(setkey, **kwargs)

        redis_list = redis.smembers(setkey)
        value_list = cls.restore_redis_list(setkey, redis_list, **kwargs)
        return value_list

#        kwargs['embedded'] = True
#        for item in redis_list:
#            obj.append(cls.restore_object(item,**kwargs))
#            
#        return obj
    
    @classmethod
    def restore_zset (cls, zsetkey, **kwargs):
        
#        obj = []
        
        #RETRIEVE VALUES IN REDIS FOR EACH ITEM IN LIST
        page_range = cls.paging_indexes(**kwargs) 
        redis_list = redis.zrange(zsetkey, page_range[0], page_range[1])#0,  -1)

        hashkeys = []
        for hashkey in redis_list:
            if cls.filter_object(hashkey, **kwargs):
                hashkeys.append(hashkey)
        
        return cls.restore_redis_list(zsetkey, hashkeys, **kwargs)
#        kwargs['embedded'] = True
#        for item in redis_list:
#            obj.append(cls.restore_object(item,**kwargs))
#            
#        return obj
    @classmethod
    def restore_redis_list (cls, key, redis_list, **kwargs):
        
        objects = []
#        parent_resources = cls.resource_tree(key)[:-1]
        
        
        kwargs['embedded'] = True
#        parents = kwargs.get('parents',[])[:]
        
        #REMOVE PAGE/OFFSET KWARGS
        if kwargs.get('page') is not None:
            kwargs.pop('page')
        if kwargs.get('offset') is not None:
            kwargs.pop('offset')
        
        for item in redis_list:
#            resource_tree = cls.resource_tree(item)
#            if len(resource_tree) >= 1:
#                new_parent = resource_tree[-1] #last index is resource for key
#                
#                if parents is not None and new_parent in parents: #If new resource already exists, it is recursive return key
#                    objects.append(item)
#                    continue
                
            #Do not restore recursive relationships
#            if cls.datatype(item) == 'hash':
#                item_resource = cls.resource_tree(item)[-1] 
#                if item_resource in parent_resources:
#                    objects.append(item)
#                    continue
            
            
            
            objects.append(cls.restore_object(item,**kwargs))
            
        return objects
    
    @classmethod
    def restore_hash (cls, hashkey, **kwargs):
        
        obj = {}
    
        #RETRIEVE VALUES IN REDIS HASH
        
        h = redis.hgetall(hashkey)
        keys = list(h.keys())#redis.hkeys(hashkey) #TODO: REMOVE AND DO Hgetall only
        if len(keys) > 0:
            obj['hashkey'] = hashkey #Add hashkey value to obj
        
        #Check for Skip Include -> This should not be passed by callers as it is internal
        skip_include = kwargs.get('_skip_include')
        if skip_include is not None:
            kwargs.pop('_skip_include') #Don't pass to nested objects.
        
        kwargs['embedded'] = True
        
        
        
        i = 0
        for key in keys:
            i += 1
            #Include fields: Skip all fields that are not explicitly included -> Takes precedence over excluded & nested styles
            included_fields = kwargs.get('include')
            if included_fields is not None and skip_include is None:
                #Commented out next line because we do need hashkey returned
#                if 'hashkey' not in included_fields and 'hashkey' in obj:
#                    obj.pop('hashkey') #Don't let hashkey sneak in just cause it was added to obj outside the loop
                if key not in included_fields:
                    if i == len(keys) and (obj == {} or 
                                           (len(obj)==1 and obj.get('hashkey') is not None)): #Included fields blocked all fields in this object: redo and allow all fields.
                        kwargs['_skip_include'] = 'poop'
                        return cls.restore_hash(hashkey, **kwargs) #Rerun restore_hash and include all fields for current object.
                    continue
            
            #Exclude fields: Skip fields that are explicitly excluded
            excluded_fields = kwargs.get('exclude')
            if excluded_fields is not None and key in excluded_fields:
                continue
            
            #Add key after data validation
            kw = h[key]
            if kw is None: continue
            if kw == hashkey: 
                continue #otherwise, infinite loop
            
            kwargs['resource'] = key

            dt = cls.datatype(key, **kwargs)
            if dt == 'value':
                obj[key] = kw
            else:
                obj[key] = cls.restore_object(kw, **kwargs)

            # obj[key] = cls.restore_object(kw,**kwargs)
        
        
        return obj
    
    
    @classmethod
    def restore_hash_old (cls, hashkey, **kwargs):
        
        obj = {}
    
        #RETRIEVE VALUES IN REDIS HASH
        keys = redis.hkeys(hashkey) #TODO: REMOVE AND DO Hgetall only
        if len(keys) > 0:
            obj['hashkey'] = hashkey #Add hashkey value to obj
        
        #Check for Skip Include -> This should not be passed by callers as it is internal
        skip_include = kwargs.get('_skip_include')
        if skip_include is not None:
            kwargs.pop('_skip_include') #Don't pass to nested objects.
        
        kwargs['embedded'] = True
        
        
        
        
        
        
        
        
        
        i = 0
        h = redis.hgetall(hashkey)
        for key in keys:
            i += 1
            #Include fields: Skip all fields that are not explicitly included -> Takes precedence over excluded & nested styles
            included_fields = kwargs.get('include')
            if included_fields is not None and skip_include is None:
                if 'hashkey' not in included_fields and 'hashkey' in obj:
                    obj.pop('hashkey') #Don't let hashkey sneak in just cause it was added to obj outside the loop
                if key not in included_fields:
                    if i == len(keys) and obj == {}: #Included fields blocked all fields in this object: redo and allow all fields.
                        kwargs['_skip_include'] = 'poop'
                        return cls.restore_hash(hashkey, **kwargs) #Rerun restore_hash and include all fields for current object.
                    continue
            
            #Exclude fields: Skip fields that are explicitly excluded
            excluded_fields = kwargs.get('exclude')
            if excluded_fields is not None and key in excluded_fields:
                continue
            
            #Add key after data validation
            #kw = redis.hget(hashkey,key) #DEPRECATED --> HITS REDIS FOR EVERY KEY.  DO HGETALL ABOVE
            kw = h[key]
            if kw is None: continue
            if kw == hashkey: 
                continue #otherwise, infinite loop
            
            #Create nested key for nested styles
            resources = cls.resource_tree(hashkey, style='resource-list')
            nested_key = ''
            for r in resources:
                nested_key += r + '.'
            nested_key += key
            parent_key = resources[-1] + '.' + key
            
#            _recursive = False
#            if key in resources:  #Recursive object found
#                _recursive = True
            
            #Do not restore recursive relationships
#            parent_resources = cls.resource_tree(hashkey)
#            if cls.datatype(kw) == 'hash':
#                kw_resource = cls.resource_tree(kw)[-1] 
#                if kw_resource in parent_resources:
#                    obj[key] = kw
#                    continue
            
            #Nested Styles:
            nested_styles = kwargs.get('nested_styles')
            
            if nested_styles is not None:# and (key in nested_styles or parent_key in nested_styles or nested_key in nested_styles):
                #Determine style key
                style_key = None
                if key in nested_styles:
                    style_key = key
                elif parent_key in nested_styles:
                    style_key = parent_key
                elif nested_key in nested_styles:
                    style_key = nested_key
                
                if style_key is not None:
                    style = nested_styles[style_key]
                else:
#                    if key == 'posts':
#                        f = s
                    style = 'verbose' 
                
                #style = nested_styles[style_key]
                
                datatype = cls.datatype(kw)
                
                first = 0
                last = -1
                components = style.split('_')
                if len(components) > 1:
                    style = components[0]
                    index_range = components[1].split('-')
                    if len(index_range) > 1:
                        first = int(index_range[0])
                        last = int(index_range[1])
                    
                
                if style == 'hashkey':
                    if datatype == 'set':
                        obj[key] = redis.smembers(kw) #TODO: SUPPORT RANGES SOMEHOW
                    elif datatype == 'list':
                        obj[key] = redis.lrange(kw,first,last)#0,-1)
                    elif datatype == 'zset':
                        obj[key] = redis.zrange(kw,first,last)#0,-1)
                    else:
                        obj[key] = kw
                        
                elif style == 'setkey':
                    if datatype == 'set' or datatype == 'zset' or datatype == 'list':
                        obj[key] = kw
                        
                elif style == 'id':
                    if datatype == 'set' or datatype == 'zset' or datatype == 'list':
                        hks = []
                        if datatype == 'set':
                            hks = redis.smembers(kw)
                        elif datatype == 'zset':
                            hks = redis.zrange(kw,first,last)#0,-1)
                        elif datatype == 'list':
                            hks = redis.lrange(kw,first,last)#0,-1)
                        
                        ids = []  
                        for hk in hks:
                            ids.append(hk.split(':')[-1].split('|')[0])
                        obj[key] = ids
                         
                    else:#kw is a hashkey
                        obj[key] = [kw.split(':')[-1].split('|')[0]]
                        
                elif style == 'count':
                    #only supports sets for now (implement type check)
                    if datatype == 'set':
                        obj[key] = redis.scard(kw)
                    elif datatype == 'zset':
                        obj[key] = redis.zcard(kw)
                    elif datatype == 'list':
                        obj[key] = redis.llen(kw)
                        
                elif style in ['url','link','seturl']:
                    if datatype == 'hash' and style == 'url':
                            obj[key] = cls.object_url(kw, **kwargs)
                    
                    elif (datatype == 'set' or datatype == 'zset') and style != 'link': 
                        if style == 'url': #Loop through each hashkey and generate url.
                            urls = []
                            hks = []
                            if datatype == 'set':
                                hks = redis.smembers(kw)
                            elif datatype == 'zset':
                                hks = redis.zrange(kw,first,last)#0,-1)
                            elif datatype == 'list':
                                hks = redis.lrange(kw,first,last)#0,-1)
                                
                            for hk in hks:
                                urls.append(cls.object_url(hk, **kwargs))
                                obj[key] = urls
                                
                        elif style == 'seturl':
                            obj[key] = cls.set_url(kw, **kwargs)
                            
                    else: #Not a nested object or set, append url of obj
                        obj[key] = cls.restore_object(kw,**kwargs)
                        #if datatype != 'set':# and isinstance(obj[key],dict):# and obj[key].get('url') is None:
                        if isinstance(obj[key],dict):
                            obj[key]['url'] = cls.object_url(kw, **kwargs)
#                        if obj.get('url') is None:
#                            obj['url'] = cls.object_url(hashkey, **kwargs)

                         
#                elif style == 'url':
#                    if datatype == 'hash':
#                        obj[key] = cls.object_url(kw, **kwargs)
#                    elif datatype == 'set': #If a set, loop through each hashkey and generate url.
#                        urls = []
#                        hks = redis.smembers(kw)
#                        for hk in hks:
#                            urls.append(cls.object_url(hk, **kwargs))
#                        obj[key] = urls
#                    else: #Not a nested object or set, append url of obj
#                        obj[key] = cls.restore_object(kw,**kwargs)
#                        if obj.get('url') is None:
#                            obj['url'] = cls.object_url(hashkey, **kwargs)
#                        
#                elif style == '+url':
#                    obj[key] = cls.restore_object(kw,**kwargs)
#                    if obj.get('url') is None:
#                        obj['url'] = cls.object_url(hashkey, **kwargs)
#                        #no need to check datatype right?...this is a hash
##                        dt = cls.datatype(hashkey)
##                        if dt == 'hash':
##                            obj['url'] = cls.object_url(hashkey, **kwargs)
##                        elif dt == 'set': #wouldn't happen right?...this is hash
##                            obj['url'] = cls.set_url(hashkey, **kwargs)
#                    
#                elif style == 'seturl':
##                    if datatype == 'hash':
##                        obj[key] = cls.object_url(kw, **kwargs)
#                    if datatype == 'set':
#                        obj[key] = cls.set_url(kw, **kwargs)
#                    else: #Not a nested object or set, append url of obj
#                        obj[key] = cls.restore_object(kw,**kwargs)
#                        if obj.get('url') is None:
#                            obj['url'] = cls.object_url(hashkey, **kwargs)
                elif style == 'verbose':
                    kwargs['datatype'] = datatype
#                    if _recursive:
#                        kwargs['style'] = 'hashkey' #OVERRIDE verbose style for recursive relationships
                    if first != 0 or last != -1:
                        kwargs['offset'] = first
                        kwargs['limit'] = last-first
                    obj[key] = cls.restore_object(kw,**kwargs)
                    
#                else:
#                    continue #Excludes the key if it doesn't match a known style
                
                
                #TODO: ADD SUPPORT FOR URL, +URL, SETURL, and support ZSETS & LISTS for most styles

                continue
            else:
                obj[key] = cls.restore_object(kw,**kwargs)
                style = kwargs.get('style')
                nested_obj = obj[key]
                if style in ['url','link','seturl'] and isinstance(nested_obj,dict):#and obj[key].get('url') is None:
                    obj[key]['url'] = cls.object_url(kw, **kwargs)
        
        
        return obj
    
    
    @classmethod
    def datatype (cls, key, **kwargs):
        """
        Returns Redis datatype for a key
        """
        if key is None or not isinstance(key,str):
            return None
        elif isinstance(key,str) and key.strip() == '':
            return None

        if kwargs.get('datatypes') is not None and isinstance(kwargs.get('datatypes'),dict):
            datatypes = kwargs.get('datatypes')
            if datatypes.get(key) is not None and isinstance(datatypes.get(key),str):
                return datatypes.get(key)
        
        keyparts = key.split('|')
        if len(keyparts) > 1:
            #Datatype appended
            datatype = keyparts[-1]
            if datatype == 's':
                return 'set'
            elif datatype == 'z':
                return 'zset'
            elif datatype == 'l':
                return 'list'
            elif datatype == 'h':
                return 'hash'
            elif datatype == 'v':
                return 'value'
        
        dt = redis.type(key)
        if dt == 'none':
            return None
        return dt
    
    @classmethod
    def resource_tree (cls, hashkey, **kwargs):
        """
        Returns array of resources in a hash key.
        
        Optionally specify style kwarg to receive alternative formats, e.g. dict will return a dict with resources as keys and ids as vals
        
        """
        
        style = kwargs.get('style','resource-list')
        
        resource = ''
        resource_tree = []#{}
        components = hashkey.split(':')
        for c in components:
            #check for/remove type from id:
            pieces = c.split('|')
            p = pieces[0]
            if len(pieces) == 1:
                p = p.lower()
                resource = p
            else:
                resource_tree.append({resource:p})
#                resource_tree[resource] = p
        
        if style == 'resource-dict':
            return resource_tree
        elif style == 'resource-list':
            resources = []
            for r in resource_tree:
                resource = list(r.keys())[0]
                resources.append(resource)
            return resources
#            l = list(resource_tree.keys())
#            if len(l) > 0:
#                l = l.reverse()
#            return l
        elif style == 'id-list':
            ids = []
            for r in resource_tree:
                resource = r.keys()[0]
                ids.append(r[resource])
#            for k in resource_tree.keys():
#                ids.append(resource_tree[k])
            return ids
        
        
    @classmethod
    def object_url (cls, hashkey, **kwargs):
        """
        Generates Resource URL from key
        """
        
        url = 'https://allysian.herokuapp.com/v1/'
        
        components = hashkey.split(':')
        
        for c in components:
            #check for/remove type from id:
            pieces = c.split('|')
            p = pieces[0]
            if len(pieces) == 1:
                p = p.lower()#lower case for resource types
            url += p + '/'
            
        return url
    
    @classmethod
    def set_url (cls, setkey, **kwargs):
        """
        Generates Resource URL from key (EITHER SETKEY OR ZSETKEY)
        """
        
        url = 'https://allysian.herokuapp.com/v1/'
        
        components = setkey.split(':')[:-1] #Remove last component due to extra ':'
        
        i = 0
        for c in components:
            #check for/remove type from id:
            pieces = c.split('|')
            p = pieces[0]
            if len(pieces) == 1:
                p = p.lower()#lower case for resource types
            
            i += 1 #Check for last entry
            if i == len(components):
                if p[-4:] == '_set':
                    p = p[:-4]
                elif p[-5:] == '_zset':
                    p = p[:-5]
                    
            url += p + '/'
        return url
    
    @classmethod
    def list_objects (cls, **kwargs):
        """
        Lists a set of hashkeys, with verbose option
        
        Required Kwargs: setkey or zsetkey
        
        Optional Kwargs: 
        -- TO DEPRECTE: verbose (if passed, method returns list of meme objs)
        -- style= default|verbose|hashkey|id|url
        
        """
#        for key in kwargs.keys():
#            if key == 'verbose':
#                verbose = True
        
        #Check for set/zset & setup objs  TODO add listkey
        objs = []
        if kwargs.get('setkey') is not None:
            objs = redis.smembers(kwargs.pop('setkey'))
        elif kwargs.get('zsetkey') is not None:
            objs = redis.zrange(kwargs.pop('zsetkey'), 0,-1)
            
        #cleaner way (implement & test later)
#        objs = []
#        key = kwargs.get('key')
#        datatype = redis.type(key)
#        if datatype == 'set':
#            objs = redis.smembers(key)
#        elif datatype == 'zset':
#            objs = redis.zrange(key, 0,-1)
        
        
        
        style = kwargs.get('style','hashkey')
        
        #Check Verbosity (Legacy Param to override style) #TODO: DEPRECATE
        verbose = kwargs.get('verbose')
        if verbose is not None:
            style = 'verbose'
        
        
        #TODO: Implement id, url, terse + all return vals,
        
        if style == 'hashkey':
            envelope = kwargs.get('envelope','HASHKEYS')
            return {envelope:objs}
        elif style == 'verbose' or style in ['url','link','seturl']:
            verbose_objs = []
            for hashkey in objs:
                obj = cls.restore_object(hashkey, **kwargs)
#                                         max_depth=kwargs.get('max_depth'),
#                                         depth=1,
#                                         exclude=kwargs.get('exclude'))
                if obj is not None: #TODO: CHECK IF OBJ = {} as well, and if it is...delete it.
                    verbose_objs.append(obj)
                else:
                    continue 
                    #TODO keys that have no data should be removed from the set key. 
                    #(one way to do this could be to add a list property to every hash object that tracks which lists/sets and zsets it is a part of.  
                    #Then each time you clear a key, you could clear the lists it is a part of.  This would work for hashes but
                    # the key for a list or set could  also be in another list or set and we would not know  to remove it. 
                    #Therefore whenever accessing a list or set or zset it should be setup to remove references to keys that don't exist anymore. 
            envelope = kwargs.get('envelope','OBJECTS')
            return {envelope: verbose_objs}
        elif style == 'id':
            ids = []
            for hashkey in objs:
                resource_id = hashkey.split(':')[-1].split('|')[0]
                if resource_id is not None:
                    ids.append(resource_id)
            envelope = kwargs.get('envelope','IDS')
            return {envelope:ids}
        
        
        else:
            #Verbose top-level objects w/ nested styles:
#            try:
                nested_styles = {}
                
                #First check for style definition in styles dict:
                styles = kwargs.get('styles')
                if styles is not None and style in styles:
                    nested_styles = styles[style]
                
                #If Not found, assemble nested styles definition:
                if nested_styles == {}:
                    nested_styles = dict(e.split(':') for e in style.split('|'))
                
                
                #Restore Objects
                styled_objs = []
                kwargs['nested_styles'] = nested_styles
                for hashkey in objs: #TODO: CHECK IF OBJ = {} as well, and if it is...delete it.
                    obj = cls.restore_object(hashkey, **kwargs)
    
                    if obj is not None:
                        styled_objs.append(obj)
                    else:
                        continue
                envelope = kwargs.get('envelope','OBJECTS')
                return {envelope: styled_objs}
#            except:
#                return 'ERROR: Invalid style parameter'
        #Default to hashkeys:
        envelope = kwargs.get('envelope','HASHKEYS')
        return {envelope:objs}
    
    
    @classmethod
    def delete_object (cls, **kwargs): #CHANGED from clear_objects
        """
        Deletes an object completely in Redis by recursively deleting sub objects
        
        Required Kwargs: key
        
        Optional Kwargs: 
        
        """
        
        #IDENTIFY ALL EMBEDDED KEYS based on parent object datatype
        embedded_keys = []
#        key = kwargs.get('key')
        if kwargs.get('key') is not None:
            key = kwargs.pop('key')
            
        #Build List Of Parent Resources:
        parents = kwargs.get('parents',[])[:]
        # new_parent = cls.resource_tree(key)[-1] #last index is resource for key
        new_parent = cls.get_resource_name(key)
        
        # parents.append(new_parent)
        kwargs['parents'] = parents[:]
            
        delete_rule = kwargs.get('delete',{})
        if delete_rule is None:
            delete_rule = {}

        default_rule = kwargs.get('default_delete_rule', 'no action')
        
#        datatype = redis.type(key)
        datatype = cls.datatype(key, **kwargs)
        
        if datatype == 'set':
            embedded_keys = redis.smembers(key)
            if default_rule == 'deny' and len(embedded_keys) > 0:
                return False
            #Add all members of set to embedded keys, if keys are invalid, redis.delete will ignore them
            
        elif datatype == 'zset':
            embedded_keys = redis.zrange(key, 0,-1)
            if default_rule == 'deny' and len(embedded_keys) > 0:
                return False
            #Add all members of zset to embedded keys, if keys are invalid, redis.delete will ignore them
            
        elif datatype == 'list':
            embedded_keys = redis.lrange(key, 0,-1)
            if default_rule == 'deny' and len(embedded_keys) > 0:
                return False
            #Add all members of list to embedded keys, if keys are invalid, redis.delete will ignore them
            
        elif datatype == 'hash': 
            
            if new_parent in parents: #If new resource already exists, it is recursive return key
                redis.delete(key) #TODO: TEST THIS THOROUGHLY FOR NESTED OBJECTS ETC.
            #    pass
                return None
            
            kwargs['parents'].append(new_parent)
            hkeys = redis.hkeys(key)
            for k in hkeys:
                if k == 'hashkey':
                    continue

                v = redis.hget(key,k)
                dt = cls.datatype(v)
                #if redis.type(v) is not None:
                if cls.datatype(v) is not None and not cls.is_excluded(k, **kwargs): #Value stored in hash is an embedded redis key (e.g. a list, a set, etc.)
                    # embedded_keys.append(v)
                    rule = delete_rule.get(k, default_rule)
                    kwargs['default_delete_rule'] = rule
                    if rule == 'cascade':
                        cls.delete_object(key=v, **kwargs)
                    elif rule == 'deny':
                        if dt == 'hash':
                            return False
                        else:
                            cls.delete_object(key=v, **kwargs)
                    else:
                        if dt != 'hash':
                            redis.delete(v)
        
        # elif datatype == 'string':
        #     v = redis.get(key)
        #     dt = cls.datatype(v) #TODO LOOKINTO THIS ...recursing on None doesn't make sense
        #     if dt is not None: #redis.type(v) is not None: #Value represents another redis key
        #         embedded_keys.append(v)
                
        if default_rule == 'deny':
            if len(embedded_keys) > 0:
                return False
        #RECURSIVELY DELETE EMBEDDED KEYS
        kwargs['default_delete_rule'] = default_rule

        for k in embedded_keys: #TODO check data type of keys and make more dynamic to safely handle all possible cases
            cls.delete_object(key=k, **kwargs)

        prefix = cls.get_prifix(key)
        resource = cls.get_resource_name(key)

        prefix_zsetkey = ResourceManager.zsetkey(prefix+":", resource)
        redis.zrem(prefix_zsetkey,key)

        
        resource_zsetkey = ResourceManager.zsetkey('', resource)
        redis.zrem(resource_zsetkey,key)
                
        #DELETE PARENT KEY
        redis.delete(key)
        
        return True

    @classmethod
    def get_prifix(cls, hashkey):
        words = hashkey.split(":")

        if len(words) < 3:
            return '';
        
        return ":".join(words[:-2])

    @classmethod
    def get_resource_name(cls, hashkey):
        words = hashkey.split(":")

        if len(words) < 1:
            return None
        elif len(words) < 2:
            return words[-1]

        return words[-2]
    
    @classmethod
    def is_excluded(cls, k, **kwargs):
        """
        Returns a BOOL indicating whether key should be excluded 
        from embedded objects for get/delete
        """
        #Exclude fields: Skip fields that are explicitly excluded
        excluded_fields = kwargs.get('exclude')
        if excluded_fields is not None and k in excluded_fields:
            return True
        return False
    
    @classmethod
    def replicate_master_db(cls, **kwargs):
        
        key = kwargs.get('key')
        if key is not None:
            cls.replicate_master_key(key)
            return cls.restore_object(key)
        
        keys = redis_master.keys('*')
        for key in keys:
            cls.replicate_master_key(key)
            
        return keys
    
    @classmethod
    def replicate_master_key(cls, key):
    
        dump = redis_master.dump(key)
        redis.restore(key,0,dump)

    @classmethod
    def get_mapping_resource(cls, key, **kwargs):
        #CHECK IF key IS FOR MAPPING RESOURCES
        refs = kwargs.get('mapping')
        field = key.rsplit(':',1)[1]
        field = field.split('|')[0]
        if refs is not None and isinstance(refs, dict):
            for ref_key, resource in refs.items():
                if ref_key == field:
                    return resource
        return None

    @classmethod
    def filter_object (cls, hashkey, **kwargs):
        #CHECK OBJECT WITH HASHKEY MATCHING WITH FILTER "has_element"
        valid = True
        filters = kwargs.get("has_element")
        if filters is not None and isinstance(filters, list) and len(filters) > 0:
            valid = False
            for filter_dict in filters:
                sub_valid = True
                for field, resource_id in filter_dict.items():
                    key = hashkey + ":" + field + "|s"
                    resource = cls.get_mapping_resource(key, **kwargs)  # Why not pass field instead of key?
                    if resource is not None:
                        params = {}
                        q_hashkey = ResourceManager.hashkey('', resource, resource_id, **params)
                        sub_valid = (sub_valid and redis.sismember(key,q_hashkey))  # Why not make this 'valid = reids.sismember(key, q_hashkey)'  and remove sub_valid=True etc.
                valid = (valid or sub_valid)
        return valid
        
        

#Common is a class full of highly reusable common class methods. 
class Common ():
    
    @classmethod
    def random_str(cls, **kwargs):
        """
        Returns a random string with optional parameters to set the string size and char set.
        Defaults to a six digit string consisting of uppercase and numbers
        
        Required Kwargs: None
        
        Optional Kwargs: Size and Chars
        
        """
        #Retrieve Kwargs
        size = kwargs.get('size',6)
        chars = kwargs.get('chars', string.ascii_uppercase + string.digits)
        
        
        return ''.join(random.choice(chars) for _ in range(size))
    
    
    
    @classmethod
    def current_PST_time_str(cls, **kwargs):
        """
        Returns current PST time as a string.
        
        Note: currently does not account for daylight savings  #FIX
        """
        now =  cls.current_PST_time_dt()
        return cls.datetime_string_converter(datetime=now)
#    datetime.strftime(now,"%Y-%m-%d %H:%M")
    
    @classmethod
    def current_PST_time_dt(cls, **kwargs):
        """
        Returns current PST time as a datetime object.
        
        Note: currently does not account for daylight savings #FIX
        """
        now =  datetime.now().replace(tzinfo=utc) +timedelta(hours =-7)
        return now
    
    @classmethod
    def datetime_string_converter(cls, **kwargs):
        """
        Returns a converted date.
        If user passes optional kwarg 'string', return value is datetime object
        If user passes optional kwarg 'datetime', return value is a string
        If user passes both string and datetime, only string is evaluated
        
        Optionally, a date_format_str can be passed as well.
        
        Required Kwargs: N/A (Either string or datetime must be provided, however)
        
        Optional Kwargs:  string, datetime, date_format_str
        
        Error Notes: Returns None if user does not pass either 'string' or 'datetime' kwargs.
        """
        #Gather Kwargs
        string = kwargs.get('string')
        dt = kwargs.get('datetime')
        date_format_str = kwargs.get('date_format_str')
        
        #Error check & set defaults
        if string is None and dt is None:
            return None
        if date_format_str is None:
            date_format_str = "%Y-%m-%d %H:%M"
            
        if string is not None:
            return datetime.strptime(string,date_format_str)
        elif dt is not None:
            return datetime.strftime(dt,date_format_str)
        
        return None #Should never hit this but just in case
    
    @classmethod
    def retrieve_json(cls, url, **kwargs):
        """
        Retrieves JSON dict object at url
        """
        

        #Read URL response and load json data
        
        try:
            file = urllib2.urlopen(url, None, 5.0)
            data = file.read()
            file.close()
            return json.loads(data)
        except:
            return None
    
    @classmethod
    def is_number(cls, s):
        try:
            float(s)
            return True
        except ValueError:
            return False
